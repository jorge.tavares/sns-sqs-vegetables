package br.com.jorge.poc.snssqs.vegetables.aws.api;

import br.com.jorge.poc.snssqs.vegetables.aws.AwsLocalGateway;
import com.amazonaws.services.sns.AmazonSNS;
import lombok.AllArgsConstructor;
import org.springframework.context.annotation.Profile;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;

@RestController
@Profile("local")
@AllArgsConstructor
@RequestMapping("/vegetables/aws")
public class AwsLocalRestService {

    private final AwsLocalGateway awsLocalGateway;

    @PostMapping("/sns")
    @ResponseStatus(HttpStatus.CREATED)
    String createSnsTopic(@RequestBody CreateTopicRequest createTopicRequest) {
        return awsLocalGateway.crateTopic(createTopicRequest.getTopicName());
    }

    @PostMapping("/sns/subscribe")
    @ResponseStatus(HttpStatus.OK)
    String subscribeTopic(@RequestBody SubscribeTopicRequest subscribeTopicRequest) {
        return awsLocalGateway.subscribeTopic(subscribeTopicRequest.getTopicArn(),
                subscribeTopicRequest.getProtocol(),
                subscribeTopicRequest.getQueueUrl());
    }

    @PostMapping("/sqs")
    @ResponseStatus(HttpStatus.CREATED)
    String createSqsQueue(@RequestBody CreateQueueRequest createQueueRequest) {
        return awsLocalGateway.createQueue(createQueueRequest.getQueueName());
    }
}
