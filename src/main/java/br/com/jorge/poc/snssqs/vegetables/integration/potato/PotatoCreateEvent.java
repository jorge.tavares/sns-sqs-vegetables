package br.com.jorge.poc.snssqs.vegetables.integration.potato;

import br.com.jorge.poc.snssqs.vegetables.core.DomainEvent;
import br.com.jorge.poc.snssqs.vegetables.potato.Potato;
import lombok.*;

import static lombok.AccessLevel.PRIVATE;

@Getter
@Builder
@EqualsAndHashCode
@AllArgsConstructor
@NoArgsConstructor(access = PRIVATE)
public class PotatoCreateEvent implements DomainEvent {

    private String uuid;

    private String description;

    public static PotatoCreateEvent of(Potato potato) {
        return PotatoCreateEvent.builder()
                .uuid(potato.getUuid())
                .description(potato.getDescription())
                .build();
    }

    @Override
    public String getEventType() {
        return "potato-create";
    }
}
