package br.com.jorge.poc.snssqs.vegetables.aws;

public interface AwsLocalGateway {
    String crateTopic(String topicName);
    String createQueue(String queueName);
    String subscribeTopic(String topicArn, String protocol, String queueUrl);
}
