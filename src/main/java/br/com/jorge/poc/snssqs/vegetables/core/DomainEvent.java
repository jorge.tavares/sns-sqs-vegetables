package br.com.jorge.poc.snssqs.vegetables.core;

import com.fasterxml.jackson.annotation.JsonIgnore;

public interface DomainEvent {
    @JsonIgnore
    String getEventType();
}
