package br.com.jorge.poc.snssqs.vegetables.potato;

import lombok.*;
import org.springframework.data.mongodb.core.mapping.Document;
import org.springframework.data.mongodb.core.mapping.MongoId;

import static lombok.AccessLevel.PRIVATE;

@Getter
@Builder
@EqualsAndHashCode
@AllArgsConstructor
@Document(collection = "potato")
@NoArgsConstructor(access = PRIVATE)
public class Potato {

    @MongoId
    private String uuid;

    private String description;
}
