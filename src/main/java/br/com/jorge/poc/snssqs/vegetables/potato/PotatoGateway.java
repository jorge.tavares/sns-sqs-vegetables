package br.com.jorge.poc.snssqs.vegetables.potato;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.web.bind.annotation.PostMapping;

public interface PotatoGateway {
    Potato producePotato(Potato potato);
    Potato savePotato(Potato potato);
    Potato findPotatoById(String id);
    Page<Potato> findAllPotato(Pageable pageable);
}
