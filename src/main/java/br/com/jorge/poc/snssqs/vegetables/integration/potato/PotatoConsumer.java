package br.com.jorge.poc.snssqs.vegetables.integration.potato;

import lombok.extern.slf4j.Slf4j;
import org.springframework.cloud.aws.messaging.listener.annotation.SqsListener;
import org.springframework.stereotype.Component;

@Slf4j
@Component
public class PotatoConsumer {

    @SqsListener("${aws.sqs.vegetables-queue-name}")
    public void queueListener(String message) {
        log.info("Potato created received: " + message);
    }

}
