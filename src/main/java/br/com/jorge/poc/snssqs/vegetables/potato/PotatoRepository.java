package br.com.jorge.poc.snssqs.vegetables.potato;

import org.springframework.data.mongodb.repository.MongoRepository;

interface PotatoRepository extends MongoRepository<Potato, String> {}
