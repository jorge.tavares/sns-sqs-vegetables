package br.com.jorge.poc.snssqs.vegetables.aws.api;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class CreateTopicRequest {
    private String topicName;
}
