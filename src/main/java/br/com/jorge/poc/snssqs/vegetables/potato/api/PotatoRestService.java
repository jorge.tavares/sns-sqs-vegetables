package br.com.jorge.poc.snssqs.vegetables.potato.api;

import br.com.jorge.poc.snssqs.vegetables.potato.Potato;
import br.com.jorge.poc.snssqs.vegetables.potato.PotatoGateway;
import lombok.AllArgsConstructor;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.web.bind.annotation.*;

import static org.springframework.http.HttpStatus.CREATED;
import static org.springframework.http.HttpStatus.OK;

@RestController
@AllArgsConstructor
@RequestMapping(value = "/vegetables/potatos")
class PotatoRestService {

    private final PotatoGateway potatoGateway;

    @PostMapping("/async")
    @ResponseStatus(CREATED)
    Potato createAsync(@RequestBody Potato potato) {
        return potatoGateway.producePotato(potato);
    }

    @PostMapping
    @ResponseStatus(CREATED)
    Potato create(@RequestBody Potato potato) {
        return potatoGateway.savePotato(potato);
    }

    @GetMapping("/{id}")
    @ResponseStatus(OK)
    Potato getById(@PathVariable String id) {
        return potatoGateway.findPotatoById(id);
    }

    @GetMapping
    @ResponseStatus(OK)
    Page<Potato> getAll(@RequestParam(defaultValue = "0", required = false) Integer page,
                        @RequestParam(defaultValue = "20", required = false) Integer size) {
        return potatoGateway.findAllPotato(PageRequest.of(page, size));
    }
}
