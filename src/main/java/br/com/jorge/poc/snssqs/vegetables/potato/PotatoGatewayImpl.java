package br.com.jorge.poc.snssqs.vegetables.potato;

import br.com.jorge.poc.snssqs.vegetables.core.SnsNotificationSender;
import br.com.jorge.poc.snssqs.vegetables.core.exception.ResourceNotFoundException;
import br.com.jorge.poc.snssqs.vegetables.integration.potato.PotatoCreateEvent;
import lombok.AllArgsConstructor;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

@Service
@AllArgsConstructor
class PotatoGatewayImpl implements PotatoGateway {

    private final PotatoRepository potatoRepository;
    private final SnsNotificationSender<PotatoCreateEvent> snsNotificationSender;

    @Override
    public Potato producePotato(Potato potato) {
        PotatoCreateEvent potatoCreateEvent = PotatoCreateEvent.of(potato);
        snsNotificationSender.send("vegetables", potatoCreateEvent);
        return potato;
    }

    @Override
    public Potato savePotato(Potato potato) {
        return potatoRepository.save(potato);
    }

    @Override
    public Potato findPotatoById(String id) {
        return potatoRepository.findById(id)
                .orElseThrow(() -> new ResourceNotFoundException("Potato not found for " + id));
    }

    @Override
    public Page<Potato> findAllPotato(Pageable pageable) {
        return potatoRepository.findAll(pageable);
    }
}
