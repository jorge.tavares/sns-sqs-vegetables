package br.com.jorge.poc.snssqs.vegetables.core;

import com.amazonaws.services.sns.AmazonSNS;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cloud.aws.messaging.core.NotificationMessagingTemplate;
import org.springframework.stereotype.Component;

@Slf4j
@Component
public class SnsNotificationSender<T> {

    private final NotificationMessagingTemplate notificationMessagingTemplate;

    @Autowired
    public SnsNotificationSender(AmazonSNS sns) {
        this.notificationMessagingTemplate = new NotificationMessagingTemplate(sns);
    }

    public void send(String topicName, T payload) {
        this.notificationMessagingTemplate.convertAndSend(topicName, payload);
    }
}