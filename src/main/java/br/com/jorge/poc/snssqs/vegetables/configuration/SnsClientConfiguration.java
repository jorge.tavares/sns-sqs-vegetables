package br.com.jorge.poc.snssqs.vegetables.configuration;

import br.com.jorge.poc.snssqs.vegetables.core.property.AwsProperties;
import br.com.jorge.poc.snssqs.vegetables.core.property.AwsSnsProperties;
import com.amazonaws.auth.DefaultAWSCredentialsProviderChain;
import com.amazonaws.client.builder.AwsClientBuilder;
import com.amazonaws.services.sns.AmazonSNS;
import com.amazonaws.services.sns.AmazonSNSClientBuilder;
import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Slf4j
@Configuration
@AllArgsConstructor
public class SnsClientConfiguration {

    private final AwsSnsProperties snsProperties;
    private final AwsProperties awsProperties;

    @Bean
    public AmazonSNS sns() {
        log.debug("Creating the SNS Client...");
        return AmazonSNSClientBuilder.standard()
                .withEndpointConfiguration(getSNSEndpointConfiguration())
                .withCredentials(DefaultAWSCredentialsProviderChain.getInstance())
                .build();
    }

    private AwsClientBuilder.EndpointConfiguration getSNSEndpointConfiguration() {
        return new AwsClientBuilder.EndpointConfiguration(snsProperties.getEndpoint(), awsProperties.getRegion());
    }

}
