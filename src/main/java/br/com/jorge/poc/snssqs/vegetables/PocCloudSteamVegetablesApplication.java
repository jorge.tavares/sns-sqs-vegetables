package br.com.jorge.poc.snssqs.vegetables;

import lombok.extern.slf4j.Slf4j;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@Slf4j
@SpringBootApplication
public class PocCloudSteamVegetablesApplication {
	public static void main(String[] args) {
		SpringApplication.run(PocCloudSteamVegetablesApplication.class, args);
	}
}
