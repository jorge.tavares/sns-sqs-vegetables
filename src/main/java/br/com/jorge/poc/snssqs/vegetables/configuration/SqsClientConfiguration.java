package br.com.jorge.poc.snssqs.vegetables.configuration;

import br.com.jorge.poc.snssqs.vegetables.core.property.AwsProperties;
import br.com.jorge.poc.snssqs.vegetables.core.property.AwsSqsProperties;
import com.amazonaws.auth.AWSStaticCredentialsProvider;
import com.amazonaws.auth.BasicAWSCredentials;
import com.amazonaws.auth.DefaultAWSCredentialsProviderChain;
import com.amazonaws.client.builder.AwsClientBuilder;
import com.amazonaws.services.sqs.AmazonSQS;
import com.amazonaws.services.sqs.AmazonSQSClientBuilder;
import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Profile;

@Slf4j
@Configuration
@AllArgsConstructor
public class SqsClientConfiguration {

    private final AwsSqsProperties sqsProperties;
    private final AwsProperties awsProperties;

    @Bean
    public AmazonSQS sqs() {
        log.info("Creating SQS Client...");
        return AmazonSQSClientBuilder.standard()
                .withEndpointConfiguration(getSQSEndpointConfiguration())
                //.withCredentials(DefaultAWSCredentialsProviderChain.getInstance())
                .withCredentials(new AWSStaticCredentialsProvider(new BasicAWSCredentials("x", "x")))
                .build();
    }

    private AwsClientBuilder.EndpointConfiguration getSQSEndpointConfiguration() {
        log.info("sqs endpoint: " + sqsProperties.getEndpoint() + " region: " + awsProperties.getRegion());
        return new AwsClientBuilder.EndpointConfiguration(sqsProperties.getEndpoint(), awsProperties.getRegion());
    }

}
