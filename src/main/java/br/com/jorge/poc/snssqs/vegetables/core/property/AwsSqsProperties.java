package br.com.jorge.poc.snssqs.vegetables.core.property;

import lombok.Data;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.stereotype.Component;

@Data
@Component
@ConfigurationProperties(prefix = "aws.sqs")
public class AwsSqsProperties {
    private String endpoint;
    private String vegetableQueueName;
}
