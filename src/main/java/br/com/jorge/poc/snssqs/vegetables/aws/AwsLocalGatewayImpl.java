package br.com.jorge.poc.snssqs.vegetables.aws;

import com.amazonaws.services.sns.AmazonSNS;
import com.amazonaws.services.sns.model.CreateTopicResult;
import com.amazonaws.services.sns.model.SubscribeRequest;
import com.amazonaws.services.sns.model.SubscribeResult;
import com.amazonaws.services.sns.model.Topic;
import com.amazonaws.services.sqs.AmazonSQS;
import com.amazonaws.services.sqs.model.CreateQueueResult;
import lombok.AllArgsConstructor;
import org.springframework.stereotype.Service;

@Service
@AllArgsConstructor
class AwsLocalGatewayImpl implements AwsLocalGateway {

    private final AmazonSNS sns;
    private final AmazonSQS sqs;

    @Override
    public String crateTopic(String topicName) {
        CreateTopicResult topic = sns.createTopic(topicName);
        return topic.getTopicArn();
    }

    @Override
    public String createQueue(String queueName) {
        CreateQueueResult queue = sqs.createQueue(queueName);
        return queue.getQueueUrl();
    }

    @Override
    public String subscribeTopic(String topicArn, String protocol, String queueUrl) {
        SubscribeResult subscribe = sns.subscribe(new SubscribeRequest(topicArn, protocol, queueUrl));
        return subscribe.getSubscriptionArn();
    }

}
